from core.ui.WebUIElement import WebUIElement as UIElement
from core.ui.By import By

def getLoginButton():
    return UIElement(By.ID,'login-button')

def getInputUsername():
    return UIElement(By.ID, 'user-name')

def getInputPassword():
    return UIElement(By.ID, 'password')

def getLoginButton():
    return UIElement(By.ID,'login-button')

def getTitleProducts():
    return UIElement(By.XPATH, "//span[text()='Products']")

def getErrorMessage():
    return UIElement(By.XPATH, "//h3[@data-test='error']")
