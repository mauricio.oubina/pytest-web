from pages import ExamplePage as page
from core.steps.BaseSteps import BaseStep
from core.assertion.Assertion import Assertion

class ExampleSteps(BaseStep):

    def loginButtonIsDisplayed(self):
        button = page.getLoginButton()
        Assertion.assertTrue('Logo is not displayed', button.isDisplayed())
        return self

    def inputUsername(self, username):
        inputUsername = page.getInputUsername()
        inputUsername.setText(username)
        return self

    def inputPassword(self, password):
        inputPassword = page.getInputPassword()
        inputPassword.setText(password)
        return self

    def clickLogin(self):
        loginButton = page.getLoginButton()
        loginButton.click()
        return self

    def validatePageProducts(self):
        title = page.getTitleProducts()
        Assertion.assertTrue('Products title is noy displayed', title.isDisplayed())
        Assertion.assertEquals('Title is not Products', 'PRODUCTS', title.getText())
        return self

    def validateErrorMessage(self, message):
        mensaje = page.getErrorMessage()
        Assertion.assertTrue('Error message is not displayed', mensaje.isDisplayed())
        Assertion.assertEquals('Messages are not equal',message, mensaje.getText())
        return self
