Feature: Example

  @example
  Scenario: User logs in CRM Application with valid credentials
    Then valida el boton de login

  @example
  Scenario Outline: User logs in App
    Given el usuario esta en la pantalla de login
    When el usuario ingresa el username <username>
    And el usuario ingresa la password <password>
    And hace click en el boton Login
    Then el usuario ingresa a la pagina de Products

    Examples:
      | username      | password     |
      | standard_user | secret_sauce |
      | problem_user  | secret_sauce |

  @example
  Scenario Outline: Invalidad username or password for login
    Given el usuario esta en la pantalla de login
    When el usuario ingresa el username <username>
    And el usuario ingresa la password <password>
    And hace click en el boton Login
    Then verifico que aparece el mensaje de error <mensaje>

    Examples:
      | username         | password       | mensaje                                                                   |
      | secret_sauce     | wrong_password | Epic sadface: Username and password do not match any user in this service |
      | invalid_username | secret_sauce   | Epic sadface: Username and password do not match any user in this service |
      | invalid_username | wrong_password | Epic sadface: Username and password do not match any user in this service |
      | locked_out_user  | secret_sauce   | Epic sadface: Sorry, this user has been locked out.                       |

  @example
  Scenario Outline: User logs in App with Facade
    Given se encuentra en la pantalla de home logueado con <username> y <password>

    Examples:
      | username      | password     |
      | standard_user | secret_sauce |
