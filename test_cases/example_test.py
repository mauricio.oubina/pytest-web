from pytest_bdd import then, scenarios, when, given
from steps.ExampleSteps import ExampleSteps as example
from facades import LoginFacade

scenarios('../features/Example.feature')

@given('se encuentra en la pantalla de home logueado con <username> y <password>')
def doLogin(username, password):
    LoginFacade.doLogin(username, password)

@then('valida el boton de login')
def validateLogo():
    example().loginButtonIsDisplayed()

@given('el usuario esta en la pantalla de login')
def validateLogin():
    example().loginButtonIsDisplayed()

@when('el usuario ingresa el username <username>')
def inputUsername(username):
    example().inputUsername(username)

@when('el usuario ingresa la password <password>')
def inputPassword(password):
    example().inputPassword(password)

@when('hace click en el boton Login')
def clickLogin():
    example().clickLogin()

@then('el usuario ingresa a la pagina de Products')
def validatePageProducts():
    example().validatePageProducts()

@then('verifico que aparece el mensaje de error <mensaje>')
def validatePageProducts(mensaje):
    example().validateErrorMessage(mensaje)

def teardown():
    example().closeBrowser()