from steps.ExampleSteps import ExampleSteps as stepsLogin

def doLogin(username, password):
    stepsLogin().loginButtonIsDisplayed().inputUsername(username).inputPassword(password).clickLogin().validatePageProducts()
